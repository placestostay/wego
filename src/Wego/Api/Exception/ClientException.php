<?php
/**
 * PHP wrapper around the Wego API.
 * 
 * @category  Wego
 * @package   Api
 * @link      http://bitbucket.org/placestostay/wego for the canonical source repository
 * @copyright Copyright (c) 2015 PlacesToStay (http://www.placestostay.com)
 * @license   http://www.placestostay.com/license/new-bsd New BSD License
 * @author    placestostay.com <info@placestostay.com>
 * @version   1.0.0
 */
namespace Wego\Api\Exception;

use RuntimeException;

/**
 * Wego API HTTP Client Exception
 *  
 * @category    Wego
 * @package     Api
 * @subpackage  Exception
 */
abstract class ClientException extends RuntimeException
{
    /**
     * The status code sent from the API.
     *
     * @var int
     */
    protected $statusCode;
    
    /**
     * Custom constructor overrides the PHP Exception class.
     *
     * @param int        $statusCode
     * @param string     $message
     * @param int        $code
     * @param \Exception $previous
     */
    public function __construct($statusCode, $message, $code = 0, \Exception $previous = null)
    {
        $this->statusCode = $statusCode;
        parent::__construct($message, $code, $previous);
    }
    
    /**
     * Get the status code sent from the API.
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}
