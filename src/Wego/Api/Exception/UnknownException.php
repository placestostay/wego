<?php
/**
 * PHP wrapper around the Wego API.
 * 
 * @category  Wego
 * @package   Api
 * @link      http://bitbucket.org/placestostay/wego for the canonical source repository
 * @copyright Copyright (c) 2015 PlacesToStay (http://www.placestostay.com)
 * @license   http://www.placestostay.com/license/new-bsd New BSD License
 * @author    placestostay.com <info@placestostay.com>
 * @version   1.0.0
 */
namespace Wego\Api\Exception;

/**
 * Wego API Unknown Exception
 *  
 * @category    Wego
 * @package     Api
 * @subpackage  Exception
 */
class UnknownException extends ClientException
{
    
}
