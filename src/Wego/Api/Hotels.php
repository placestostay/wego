<?php
/**
 * PHP wrapper around the Wego API.
 * 
 * @category  Wego
 * @package   Api
 * @link      http://bitbucket.org/placestostay/wego for the canonical source repository
 * @copyright Copyright (c) 2015 PlacesToStay (http://www.placestostay.com)
 * @license   http://www.placestostay.com/license/new-bsd New BSD License
 * @author    placestostay.com <info@placestostay.com>
 * @version   1.0.0
 */
namespace Wego\Api;

use Psr\Http\Message\ResponseInterface;
use Wego\Api\Client\ClientInterface;
use Wego\Api\Exception\AuthorizationException;
use Wego\Api\Exception\UnknownException;
use Wego\Api\Exception\QueryException;

/**
 * Wego Hotel API.
 *  
 * @category Wego
 * @package  Api
 */
class Hotels
{
    /**
     * The Wego Hotels API url.
     *
     * @var string
     */
    private $endpoint = 'http://api.wego.com/hotels/api/';
    
    /**
     * The Wego API Key is obtained by contacting sales@wego.com.
     *
     * @var string
     */
    private $apiKey;
    
    /**
     * The Wego TS Code is obtained by contacting sales@wego.com.
     *
     * @var string
     */
    private $tsCode;
    
    /**
     * The Wego supported locales/languages.
     *
     * @var array
     */
    private $locales = [
        'en', 'ar', 'bn', 'de', 'es-419', 'es', 'fa', 'fr', 'gu', 'hi', 
        'id', 'it', 'ja', 'kn', 'ko', 'ml', 'mr', 'ms', 'my', 'nl', 'or',
        'pa', 'pl', 'pt-br', 'pt', 'ru', 'sv', 'ta', 'te', 'th', 'tl',
        'tr', 'ur', 'vi', 'zh-cn', 'zh-hk', 'zh-tw'
    ];
    
    /**
     * Construct a new Hotel API Object.
     *
     * @param string  $apiKey
     * @param string  $tsCode
     * @param Request $client
     * @param array   $options
     */
    public function __construct($apiKey, $tsCode, ClientInterface $client, array $options = [])
    {
        $this->apiKey = $apiKey;
        $this->tsCode = $tsCode;
        $this->client = $client;
        $this->setOptions($options);
    }
    
    /**
     * Set the request options before calling any other public method.
     *
     * @param array $options
     * @return self
     */
    public function setOptions(array $options = [])
    {
        $options = array_merge([
            'base_uri'    => $this->endpoint,
            'http_errors' => false,
            'headers'     => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
                'User-Agent'   => 'placestostay.com' 
            ]
        ], $options);
        $this->client->setOptions($options);
        return $this;
    }

    /**
     * Get the underlying HTTP client object.
     *
     * @return ClientInterface
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Fetch the locations from Wego.
     *
     * @param  string|array $query Pass either the location query or an array of query options.
     * @return array
     * @throws \InvalidArgumentException
     */
    public function locations($query)
    {
        if (is_string($query)) {
            $query = ['q' => $query];
        }
        
        $defaults = [
            'lang'     => 'en',
            'page'     => 1,
            'per_page' => 10
        ];
        
        $query = array_merge($query, $defaults);
        
        if (!isset($query['q'])) {
            throw new \InvalidArgumentException('Missing query parameter.');
        }
        
        $request  = $this->request('locations/search', $query);
        $response = $this->response($request);
        
        return $response;
    }
    
    /**
     * Create a new Wego search.
     *
     * @param  array $query
     * @return array
     */
    public function create($query)
    {
        if (!isset($query['location_id'])) {
            throw new \InvalidArgumentException('Missing location id parameter.');
        }
        
        if (!isset($query['check_in'])) {
            throw new \InvalidArgumentException('Missing check in parameter.');
        }

        if (!isset($query['check_out'])) {
            throw new \InvalidArgumentException('Missing check out parameter.');
        }
        
        if (!isset($query['user_ip'])) {
            throw new \InvalidArgumentException('Missing user ip parameter.');
        }
        
        $defaults = [
            'rooms'  => 1,
            'guests' => 2
        ];
        
        $query = array_merge($defaults, $query);
        
        $request  = $this->request('search/new', $query);
        $response = $this->response($request);
        
        if (array_key_exists('search_id', $response)) {
            return $response['search_id'];
        }
        
        return $response;
    }
    
    /**
     * Start the search.
     *
     * @param  array $query
     * @return array
     */
    public function search($query)
    {
        if (is_string($query) || is_int($query)) {
            $searchId = $query;
            $query = [];
        }
        
        if (is_array($query) && isset($query['search_id'])) {
            $searchId = $query['search_id'];
            unset($query['search_id']);
        }
        
        if (empty($searchId)) {
            throw new \InvalidArgumentException('Missing search id parameter.');
        }
        
        $defaults = [
            'refresh'       => false,
            'currency_code' => 'USD',
            'sort'          => 'popularity',
            'order'         => 'asc',
            'popular_with'  => 'XX',
            'page'          => 1,
            'per_page'      => 20,
            'lang'          => 'en'
        ];
        
        $query = array_merge($defaults, $query);
        
        $request  = $this->request('search/'.$searchId, $query);
        $response = $this->response($request);
        
        return $response;
    }
    
    /**
     * Show the details of a single property.
     *
     * @param  array $query
     * @return array
     */
    public function show(array $query)
    {   
        if (isset($query['search_id'])) {
            $searchId = $query['search_id'];
            unset($query['search_id']);
        }
        
        if (empty($searchId)) {
            throw new \InvalidArgumentException('Missing search id parameter.');
        }
        
        if (!isset($query['hotel_id'])) {
            throw new \InvalidArgumentException('Missing hotel id parameter.');
        }
        
        $defaults = [
            'currency' => 'USD',
            'lang'     => 'en'
        ];
        
        $query = array_merge($defaults, $query);
        
        $request  = $this->request('search/show/'.$searchId, $query);
        $response = $this->response($request);
        
        return $response;
    }
    
    /**
     * Get the affiliate redirect information from Wego.
     *
     * @param  array $query
     * @return array
     */
    public function redirect($query)
    {
        if (is_array($query) && isset($query['search_id'])) {
            $searchId = $query['search_id'];
            unset($query['search_id']);
        }
        
        $defaults = [
            'locale' => 'en'
        ];
        
        $query = array_merge($defaults, $query);
        
        if (empty($searchId)) {
            throw new \InvalidArgumentException('Missing search id parameter.');
        }
        
        if (!isset($query['hotel_id'])) {
            throw new \InvalidArgumentException('Missing hotel id parameter.');
        }
        
        if (!isset($query['room_rate_id'])) {
            throw new \InvalidArgumentException('Missing room rate id parameter.');
        }
        
        $query = array_merge($defaults, $query);
        
        $request  = $this->request('search/redirect/'.$searchId, $query);
        $response = $this->response($request);
        
        return $response;
    }
    
    /**
     * Make a request to the Wego API.
     *
     * @param  string $path
     * @param  array  $query
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function request($path, $query)
    {
        $options = [
            'query' => array_merge($query, [
                'api_key' => $this->apiKey,
                'ts_code' => $this->tsCode
            ])
        ];
        
        return $this->client->request('GET', $path, $options);
    }
    
    /**
     * Parse the Wego response.
     *
     * @param  ResponseInterface      $response
     * @return array
     * @throws AuthorizationException On Wego authorization errors
     * @throws Exception              On any other Wego HTTP Error Code
     */
    protected function response($response)
    {
        $body = $response->getBody();
        $code = $response->getStatusCode();
        $json = (string) $body->getContents();
        $data = json_decode($json, true);
    
        if (200 !== $code) {
            switch (floor($code / 100)) {
                // Server Error
                case 5:
                    if (
                        $data['error'] === 'your search is invalid' ||
                        $data['error'] === 'A required parameter is missing'
                    ) {
                        throw new QueryException($code, $data['error']);
                    } else {
                        throw new AuthorizationException($code, $data['error']);
                    }
                    break;
                // Anything
                default:
                    throw new UnknownException($code, $data['error']);
            }
        }
        
        return $data;
    }
}
