<?php
/**
 * PHP wrapper around the Wego API.
 * 
 * @category  Wego
 * @package   Api
 * @link      http://bitbucket.org/placestostay/wego for the canonical source repository
 * @copyright Copyright (c) 2015 PlacesToStay (http://www.placestostay.com)
 * @license   http://www.placestostay.com/license/new-bsd New BSD License
 * @author    placestostay.com <info@placestostay.com>
 * @version   1.0.0
 */
namespace Wego\Api\Client;

/**
 * Guzzle Client Bridge
 *  
 * @category   Wego
 * @package    Api
 * @subpackage Client
 */
interface ClientInterface
{
    /**
     * Set the request options before calling any other public method.
     *
     * @param array $options
     * @return self
     */
    public function setOptions(array $options = []);
    
    /**
     * Make the request
     *
     * @param string $method  The HTTP method to use
     * @param string $uri     The Uri to request
     * @param array  $options The options to alter the request
     */
    public function request($method, $uri = null, array $options = []);
}
