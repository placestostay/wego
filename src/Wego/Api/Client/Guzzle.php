<?php
/**
 * PHP wrapper around the Wego API.
 * 
 * @category  Wego
 * @package   Api
 * @link      http://bitbucket.org/placestostay/wego for the canonical source repository
 * @copyright Copyright (c) 2015 PlacesToStay (http://www.placestostay.com)
 * @license   http://www.placestostay.com/license/new-bsd New BSD License
 * @author    placestostay.com <info@placestostay.com>
 * @version   1.0.0
 */
namespace Wego\Api\Client;

use GuzzleHttp\Client;

/**
 * Guzzle Client Bridge
 *  
 * @category   Wego
 * @package    Api
 * @subpackage Client
 */
class Guzzle extends Client implements ClientInterface
{
    /**
     * We override the GuzzleHttp\Client constructor to play nice
     * with our API.
     *
     * @param array $config The configuration options to bootstrap the client.
     */
    public function __construct(array $config = [])
    {
        // Override the parent
    }
    
    /**
     * We add the setOptions method to conform to the interface.
     *
     * @param  array $options The configuration options to bootstrap the client.
     * @return self
     */
    public function setOptions(array $options = [])
    {
        parent::__construct($options);
        return $this;
    }
}
